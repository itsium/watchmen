/* memory test */

var i = -1,
    casper = require('casper').create(),
    args = casper.cli.args,
    url = args[0],
    links = ['#/form', '#/list', '#/popup', '#/panel'],
    length = links.length;

var cb = function() {
    this.echo(this.getCurrentUrl());
};

casper.start(url);
while (++i < length) {
    casper.thenOpen(url + links[i]);
    casper.then(cb);
}
casper.run(function() {
    this.echo('end').exit();
});