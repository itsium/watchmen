/*
    requires: phantomjs, async
    usage: phantomjs index.js
*/

var async = require('async');

var sizes = ['1920x1080','1600x1200','1400x900','1024x768','800x600','420x360'];

var siteURL = 'http://vollzeitblogger.de';

var siteName = siteURL.replace('http://','');

siteName = siteName.replace('.','_') + '_';

var imageDir = './images/';

function scrot(sizes, callback){
    size = sizes.split('x');
    var page = require('webpage').create();
    page.viewportSize = { width: size[0], height: size[1] };
    page.zoomFactor = 1;

    // checking the viewport settings
    console.log(JSON.stringify(page.viewportSize));

    page.open(siteURL, function (status) {

        var filename = siteName + size[0] + 'x' + size[1] + '.png';

        page.render(imageDir + filename);
        page.close();
        callback.apply();
    });
}

async.eachSeries(sizes, scrot, function(err){
    if (err) console.log(err);
    console.log('done, quitting');
    phantom.exit();
});